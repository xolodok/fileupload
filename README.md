File upload
===============
File upload traits

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist xolodok/yii2-fileupload "*"
```

or add

```
"xolodok/yii2-file-upload": "*"
```

to the require section of your `composer.json` file.

Usage
-----

For example image saving method :

```php
public function saveImage()
{
    if($image = UploadedFile::getInstance($this, 'image')){
        $folders = $this->getFolders();
        if($fileName = $this->upload($image, $folders['original'])){
            $this->deleteOldImage();
            $this->image = $fileName;
            $origFile = $folders['original'] . '/' . $fileName;
            $this->thumbnail($origFile, $folders['thumb'], self::THUMB_WIDTH);
            $this->optimize($origFile, $folders['detail']);
        }
    }
}
```

To override the image save folders, add a method getUploadDirs(), for example:

```php
public function getUploadDirs()
{
    return [
        'original' => 'original',
        'detail' => 'detail',
        'thumb' => 'thumbnail',
        'new' => 'newDir',
    ];
}
```