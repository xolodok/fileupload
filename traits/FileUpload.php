<?php

namespace xolodok\fileupload\traits;

use yii\helpers\FileHelper;
use yii\web\UploadedFile;

trait FileUpload
{
    /**
     * Загрузка файла
     * @param UploadedFile $file
     * @param string $path Путь для сохранения файла
     * @param boolean $uniqueName Формирование уникального имени файла
     * @return string|boolean Имя созданного файла или false в случаи неудачи
     */
    public function upload($file, $path, $uniqueName = true)
    {
        $fileName = $uniqueName ? $this->getUniqueName($file) : ($file->name . '.' . $file->extension);
        FileHelper::createDirectory($path);

        if($file->saveAs($path . '/' . $fileName)){
            return $fileName;
        }

        return false;
    }

    /**
     * Формирование уникального имени файла
     * @param UploadedFile $file
     * @return string
     */
    protected function getUniqueName($file)
    {
        if($file instanceof UploadedFile){
            $extension = $file->extension;
        }
        else{
            $extension = pathinfo($file, PATHINFO_EXTENSION);
        }

        return substr_replace(sha1(microtime(true)), '', 12) . ".{$extension}";
    }

    /**
     * Удаление старого файла
     * @return void
     */
    public function deleteOldFile($attribute)
    {
        if(is_file($file = $this->getUploadFolder() . DIRECTORY_SEPARATOR . $this->{$attribute})){
            unlink($file);
        }
    }

    /**
     * Возвращает путь к файлу
     * @param $attribute Свойство в котором хранится имя файла
     * @return string
     */
    public function getPath($attribute)
    {
        return $this->getUploadFolder() . DIRECTORY_SEPARATOR . $this->{$attribute};
    }
}